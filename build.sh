
moonc -t . bundle/init.moon bundle/urlbundle.moon bundle/gmabundle.moon bundle/workshop.moon bundle/git.moon fs.moon init.moon util.moon vm_def.moon vm.moon commands.moon hud.moon

echo "
-- Copyright (C) 2018-2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the \"Software\"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- To Load VLL2 you can use any of these commands:
-- lua_run http.Fetch(\"https://vll.dbotthepony.ru/vll2.lua\",function(b)VLL2_FLAGS=1 RunString(b,\"VLL2\")end,function(err)print(\"VLL2\",err)end)
-- rcon lua_run \"http.Fetch([[https:]]..string.char(47)..[[/vll.dbotthepony.ru/vll/vll2.lua]],function(b)VLL2_FLAGS=1 RunString(b,[[VLL2]])end,function(err)print([[VLL2]],err)end)\"
-- http.Fetch('https://vll.dbotthepony.ru/vll2.lua',function(b)VLL2_FLAGS=1 RunString(b,'VLL2')end,function(err)print('VLL2',err)end)
-- ulx luarun \"http.Fetch('https://vll.dbotthepony.ru/vll2.lua',function(b)VLL2_FLAGS=1 RunString(b,'VLL2')end,function(err)print('VLL2',err)end)\"

-- served by cloudflare through https
-- lua_run http.Fetch(\"https://vllcf.dbotthepony.ru/vll2.lua\",function(b)VLL2_FLAGS=3 RunString(b,\"VLL2\")end,function(err)print(\"VLL2\",err)end)
-- rcon lua_run \"http.Fetch([[https:]]..string.char(47)..[[/vllcf.dbotthepony.ru/vll/vll2.lua]],function(b)VLL2_FLAGS=13 RunString(b,[[VLL2]])end,function(err)print([[VLL2]],err)end)\"
-- http.Fetch('https://vllcf.dbotthepony.ru/vll2.lua',function(b)VLL2_FLAGS=3 RunString(b,'VLL2')end,function(err)print('VLL2',err)end)
-- ulx luarun \"http.Fetch('https://vllcf.dbotthepony.ru/vll2.lua',function(b)VLL2_FLAGS=3 RunString(b,'VLL2')end,function(err)print('VLL2',err)end)\"

-- If your server for whatever reason does not trust R3 or ISRG root certs and you don't want cloudflare, use http (unsafe (very unsafe (yes it is (zero effort packet sniffing))))
-- lua_run http.Fetch(\"http://vll.dbotthepony.ru/vll2.lua\",function(b)VLL2_FLAGS=0 RunString(b,\"VLL2\")end,function(err)print(\"VLL2\",err)end)
-- rcon lua_run \"http.Fetch([[http:]]..string.char(47)..[[/vll.dbotthepony.ru/vll/vll2.lua]],function(b)VLL2_FLAGS=0 RunString(b,[[VLL2]])end,function(err)print([[VLL2]],err)end)\"
-- http.Fetch('http://vll.dbotthepony.ru/vll2.lua',function(b)VLL2_FLAGS=0 RunString(b,'VLL2')end,function(err)print('VLL2',err)end)
-- ulx luarun \"http.Fetch('http://vll.dbotthepony.ru/vll2.lua',function(b)VLL2_FLAGS=0 RunString(b,'VLL2')end,function(err)print('VLL2',err)end)\"

_G.VLL2_IS_WEB_LOADED = debug.getinfo(1).short_src == 'VLL2'

if VLL2_IS_WEB_LOADED then
	AddCSLuaFile()
end

local __cloadStatus = CompileString([===[
" > vll2.lua
cat init.lua >> vll2.lua
echo "
]===], 'vll2_web/init.lua', false)

if isstring(__cloadStatus) then
	print('UNABLE TO LOAD VLL2 CORE')
	print('LOAD CAN NOT CONTINUE')
	print('REASON:')
	print(__cloadStatus)
	return
else
	__cloadStatus()
end

VLL2.Message('Starting up...')
local ___status = CompileString([===[" >> vll2.lua
cat util.lua >> vll2.lua
echo "]===], 'vll2_web/util.lua', false)
if isstring(___status) then
	error(___status)
else
	___status()
end" >> vll2.lua

echo "___status = CompileString([===[" >> vll2.lua
cat fs.lua >> vll2.lua
echo "]===], 'vll2_web/fs.lua', false)
if isstring(___status) then
	error(___status)
else
	___status()
end" >> vll2.lua

echo "___status = CompileString([===[" >> vll2.lua
cat bundle/init.lua >> vll2.lua
echo "]===], 'vll2_web/bundle/init.lua', false)
if isstring(___status) then
	error(___status)
else
	___status()
end" >> vll2.lua

echo "___status = CompileString([===[" >> vll2.lua
cat bundle/urlbundle.lua >> vll2.lua
echo "]===], 'vll2_web/bundle/urlbundle.lua', false)
if isstring(___status) then
	error(___status)
else
	___status()
end" >> vll2.lua

echo "___status = CompileString([===[" >> vll2.lua
cat bundle/gmabundle.lua >> vll2.lua
echo "]===], 'vll2_web/bundle/gmabundle.lua', false)
if isstring(___status) then
	error(___status)
else
	___status()
end" >> vll2.lua

echo "___status = CompileString([===[" >> vll2.lua
cat bundle/workshop.lua >> vll2.lua
echo "]===], 'vll2_web/bundle/workshop.lua', false)
if isstring(___status) then
	error(___status)
else
	___status()
end" >> vll2.lua

echo "___status = CompileString([===[" >> vll2.lua
cat bundle/git.lua >> vll2.lua
echo "]===], 'vll2_web/bundle/git.lua', false)
if isstring(___status) then
	error(___status)
else
	___status()
end" >> vll2.lua

echo "___status = CompileString([===[" >> vll2.lua
cat vm_def.lua >> vll2.lua
echo "]===], 'vll2_web/vm_def.lua', false)
if isstring(___status) then
	error(___status)
else
	___status()
end" >> vll2.lua

echo "___status = CompileString([===[" >> vll2.lua
cat vm.lua >> vll2.lua
echo "]===], 'vll2_web/vm.lua', false)
if isstring(___status) then
	error(___status)
else
	___status()
end" >> vll2.lua

echo "
if CLIENT then
	___status = CompileString([===[" >> vll2.lua
	cat hud.lua >> vll2.lua
	echo "]===], 'vll2_web/hud.lua', false)
	if isstring(___status) then
		error(___status)
	else
		___status()
	end
end" >> vll2.lua

echo "___status = CompileString([===[" >> vll2.lua
cat commands.lua >> vll2.lua
echo "]===], 'vll2_web/commands.lua', false)
if isstring(___status) then
	error(___status)
else
	___status()
end

VLL2.Message('Startup finished')
hook.Run('VLL2.Loaded')
" >> vll2.lua
