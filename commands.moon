
-- Copyright (C) 2018-2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

import concommand, table, string, VLL2 from _G

sv_allowcslua = GetConVar('sv_allowcslua')

disallow = => SERVER and not game.SinglePlayer() and IsValid(@) and not @IsSuperAdmin() or CLIENT and not @IsSuperAdmin() and not sv_allowcslua\GetBool()
disallow2 = => SERVER and not game.SinglePlayer() and IsValid(@) and not @IsSuperAdmin()

if SERVER
	util.AddNetworkString('vll2_cmd_load_server')
	util.AddNetworkString('vll2_load_chained')
	util.AddNetworkString('vll2_load_chained_workshop')

autocomplete = {}

timer.Simple 0, -> http.Fetch 'https://vll.dbotthepony.ru/plist.php', (body = '', size = string.len(body), headers = {}, code = 400) ->
	return if code ~= 200
	autocomplete = [_file\Trim()\lower() for _file in *string.Explode('\n', body) when _file\Trim() ~= '']
	table.sort(autocomplete)

vll2_load = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow(ply)

	if not args[2]
		bundle = args[1]
		return VLL2.MessagePlayer(ply, 'No bundle were specified.') if not bundle
		return VLL2.MessagePlayer(ply, 'Bundle is already loading!') if not VLL2.AbstractBundle\Checkup(bundle\lower())
		fbundle = VLL2.URLBundle(bundle\lower())
		fbundle\Load()
		fbundle\Replicate()
		VLL2.MessagePlayer(ply, 'Loading URL Bundle: ' .. bundle)
	else
		local call

		call = ->
			bundle = table.remove(args, 1)
			return if not bundle

			if fbundle = VLL2.AbstractBundle\GetLoading(bundle\lower())
				fbundle\AddFinishHook(call)
			else
				fbundle = VLL2.URLBundle(bundle\lower())
				fbundle\AddFinishHook(call)
				fbundle\Load()
				fbundle\DoNotReplicate()

		if SERVER
			copy = table.Copy(args)
			hit = true

			for bundlelist in *VLL2.ReplicatedChained
				if #bundlelist == #copy
					hit = false

					for i1, value in ipairs(copy)
						if bundlelist[i1] ~= value
							hit = true
							break

					break if not hit

			table.insert(VLL2.ReplicatedChained, copy) if hit

			net.Start('vll2_load_chained')
			net.WriteTable(copy)
			net.Broadcast()

		VLL2.MessagePlayer(ply, 'Loading URL Bundles: ' .. table.concat(args, ', '))
		call()

vll2_load_silent = (ply, cmd, args) ->
	if not args[2]
		bundle = args[1]
		return VLL2.MessagePlayer(ply, 'No bundle were specified.') if not bundle
		return VLL2.MessagePlayer(ply, 'Bundle is already loading!') if not VLL2.AbstractBundle\Checkup(bundle\lower())
		fbundle = VLL2.URLBundle(bundle\lower())
		fbundle\Load()
		fbundle\DoNotReplicate()
		VLL2.MessagePlayer(ply, 'Loading URL Bundle silently (no replication): ' .. bundle)
	else
		local call

		call = ->
			bundle = table.remove(args, 1)
			return if not bundle

			if fbundle = VLL2.AbstractBundle\GetLoading(bundle\lower())
				fbundle\AddFinishHook(call)
			else
				fbundle = VLL2.URLBundle(bundle\lower())
				fbundle\AddFinishHook(call)
				fbundle\Load()
				fbundle\DoNotReplicate()

		VLL2.MessagePlayer(ply, 'Loading URL Bundles silently (no replication): ' .. table.concat(args, ', '))
		call()

if CLIENT
	net.Receive 'vll2_load_chained', ->
		copy = net.ReadTable()
		local call

		call = ->
			bundle = table.remove(copy, 1)
			return if not bundle

			if fbundle = VLL2.AbstractBundle\GetLoading(bundle\lower())
				fbundle\AddFinishHook(call)
			else
				fbundle = VLL2.URLBundle(bundle\lower())
				fbundle\AddFinishHook(call)
				fbundle\Load()
				fbundle\DoNotReplicate()

		VLL2.MessagePlayer(ply, 'Server required to load URL Bundles: ' .. table.concat(copy, ', '))
		call()

	net.Receive 'vll2_load_chained_workshop', ->
		copy = net.ReadTable()
		local call

		call = ->
			bundle = table.remove(copy, 1)
			return if not bundle
			wsid = tostring(math.floor(tonumber(bundle)))\lower()

			if fbundle = VLL2.AbstractBundle\GetLoading(wsid)
				fbundle\AddFinishHook(call)
			else
				fbundle = VLL2.WSBundle(wsid)
				fbundle\AddFinishHook(call)
				fbundle\Load()
				fbundle\DoNotReplicate()

		VLL2.MessagePlayer(ply, 'Server required to load URL Workshop bundles: ' .. table.concat(copy, ', '))
		call()

vll2_github = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow(ply)

	author = args[1]
	repository = args[2]
	branch = args[3]
	subdir = args[4]

	return VLL2.MessagePlayer(ply, 'No github username were specified') if not author
	return VLL2.MessagePlayer(ply, 'No repository name were specified') if not repository

	return VLL2.MessagePlayer(ply, 'Bundle is already loading!') if not VLL2.AbstractBundle\Checkup('github:' .. author .. '/' .. repository)
	fbundle = VLL2.GitHubBundle(author, repository, branch, subdir)
	fbundle\Load()
	fbundle\Replicate()
	VLL2.MessagePlayer(ply, 'Loading GitHub Bundle from ' .. author .. ' git repo ' .. repository .. ' at branch ' .. (branch or 'master') .. (subdir and ' with ' or ' without ') .. 'subdirectory check')

vll2_gitlab = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow(ply)

	author = args[1]
	repository = args[2]
	branch = args[3]
	subdir = args[4]

	return VLL2.MessagePlayer(ply, 'No gitlab username were specified') if not author
	return VLL2.MessagePlayer(ply, 'No repository name were specified') if not repository

	return VLL2.MessagePlayer(ply, 'Bundle is already loading!') if not VLL2.AbstractBundle\Checkup('gitlab:' .. author .. '/' .. repository)
	fbundle = VLL2.GitLabBundle(author, repository, branch, subdir)
	fbundle\Load()
	fbundle\Replicate()
	VLL2.MessagePlayer(ply, 'Loading GitLab Bundle from ' .. author .. ' git repo ' .. repository .. ' at branch ' .. (branch or 'master') .. (subdir and ' with ' or ' without ') .. 'subdirectory check')

vll2_mkautocomplete = (commandToReg) ->
	return (cmd, args) ->
		return if not args
		args = args\Trim()\lower()\Split(' ')
		last = table.remove(args)

		if #args == 0
			return [commandToReg .. ' ' .. _file for _file in *autocomplete] if last == ''
			return [commandToReg .. ' ' .. _file for _file in *autocomplete when _file\StartWith(last)]

		return [commandToReg .. ' ' .. table.concat(args, ' ') .. ' ' .. _file for _file in *autocomplete] if last == ''
		return [commandToReg .. ' ' .. table.concat(args, ' ') .. ' ' .. _file for _file in *autocomplete when _file\StartWith(last)]

vll2_urlcontent = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow2(ply)
	bundle = args[1]
	return VLL2.MessagePlayer(ply, 'No URL were specified') if not bundle
	matchStart, matchEnd = bundle\find('https?://([a-zA-Z.-/%%0-9%?=_&]+)')
	return VLL2.MessagePlayer(ply, 'Invalid URL provided') if not matchStart or matchEnd ~= #bundle
	return VLL2.MessagePlayer(ply, 'Bundle is already loading!') if not VLL2.AbstractBundle\Checkup(bundle\lower())
	fbundle = VLL2.URLGMABundle(nil, bundle)
	fbundle\DoNotLoadLua()
	fbundle\Load()
	fbundle\Replicate()
	VLL2.MessagePlayer(ply, 'Loading URL GMA without Lua: ' .. bundle)

vll2_urlgma = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow(ply)
	bundle = args[1]
	return VLL2.MessagePlayer(ply, 'No URL were specified') if not bundle
	matchStart, matchEnd = bundle\find('https?://([a-zA-Z.-/%%0-9%?=_&]+)')
	return VLL2.MessagePlayer(ply, 'Invalid URL provided') if not matchStart or matchEnd ~= #bundle
	return VLL2.MessagePlayer(ply, 'Bundle is already loading!') if not VLL2.AbstractBundle\Checkup(bundle\lower())
	fbundle = VLL2.URLGMABundle(nil, bundle)
	fbundle\Load()
	fbundle\Replicate()
	VLL2.MessagePlayer(ply, 'Loading URL GMA with Lua: ' .. bundle)

vll2_workshop = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow(ply)

	if not args[2]
		bundle = args[1]
		return VLL2.MessagePlayer(ply, 'No workshop ID were specified.') if not bundle
		return VLL2.MessagePlayer(ply, 'Bundle is already loading!') if not VLL2.AbstractBundle\Checkup(bundle\lower())
		return VLL2.MessagePlayer(ply, 'Invalid ID provided. it must be an integer') if not tonumber(bundle)
		wsid = tostring(math.floor(tonumber(bundle)))\lower()
		fbundle = VLL2.WSBundle(wsid)
		fbundle\Load()
		fbundle\Replicate()
		VLL2.MessagePlayer(ply, 'Loading Workshop Bundle: ' .. bundle)
	else
		for bundle in *args
			return VLL2.MessagePlayer(ply, 'Invalid ID provided (' .. bundle .. '). it must be an integer') if not tonumber(bundle)

		local call

		call = ->
			bundle = table.remove(args, 1)
			return if not bundle
			wsid = tostring(math.floor(tonumber(bundle)))\lower()

			if fbundle = VLL2.AbstractBundle\GetLoading(wsid)
				fbundle\AddFinishHook(call)
			else
				fbundle = VLL2.WSBundle(wsid)
				fbundle\AddFinishHook(call)
				fbundle\Load()
				fbundle\DoNotReplicate()

		if SERVER
			copy = table.Copy(args)
			hit = true

			for bundlelist in *VLL2.ReplicatedChainedWorkshop
				if #bundlelist == #copy
					hit = false

					for i1, value in ipairs(copy)
						if bundlelist[i1] ~= value
							hit = true
							break

					break if not hit

			table.insert(VLL2.ReplicatedChainedWorkshop, copy) if hit

			net.Start('vll2_load_chained_workshop')
			net.WriteTable(copy)
			net.Broadcast()

		VLL2.MessagePlayer(ply, 'Loading URL Bundles: ' .. table.concat(args, ', '))
		call()

vll2_wscollection = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow(ply)
	bundle = args[1]
	return VLL2.MessagePlayer(ply, 'No workshop ID of collection were specified.') if not bundle
	return VLL2.MessagePlayer(ply, 'Bundle is already loading!') if not VLL2.AbstractBundle\Checkup(bundle\lower())
	return VLL2.MessagePlayer(ply, 'Invalid ID provided. it must be an integer') if not tonumber(bundle)
	wsid = tostring(math.floor(tonumber(bundle)))\lower()
	fbundle = VLL2.WSCollection(wsid)
	fbundle\Load()
	fbundle\Replicate()
	VLL2.MessagePlayer(ply, 'Loading Workshop collection Bundle: ' .. bundle .. '. Hold on tigh!')

vll2_wscollection_content = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow2(ply)
	bundle = args[1]
	return VLL2.MessagePlayer(ply, 'No workshop ID of collection were specified.') if not bundle
	return VLL2.MessagePlayer(ply, 'Bundle is already loading!') if not VLL2.AbstractBundle\Checkup(bundle\lower())
	return VLL2.MessagePlayer(ply, 'Invalid ID provided. it must be an integer') if not tonumber(bundle)
	fbundle = VLL2.WSCollection(tostring(math.floor(tonumber(bundle)))\lower())
	fbundle\DoNotLoadLua()
	fbundle\Load()
	fbundle\Replicate()
	VLL2.MessagePlayer(ply, 'Loading Workshop collection Bundle: ' .. bundle .. ' without mounting Lua. Hold on tigh!')

vll2_workshop_silent = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow(ply)

	if not args[2]
		bundle = args[1]
		return VLL2.MessagePlayer(ply, 'No workshop ID were specified.') if not bundle
		return VLL2.MessagePlayer(ply, 'Bundle is already loading!') if not VLL2.AbstractBundle\Checkup(bundle\lower())
		return VLL2.MessagePlayer(ply, 'Invalid ID provided. it must be an integer') if not tonumber(bundle)
		wsid = tostring(math.floor(tonumber(bundle)))\lower()
		fbundle = VLL2.WSBundle(wsid)
		fbundle\Load()
		fbundle\Replicate()
		fbundle\DoNotReplicate()
		VLL2.MessagePlayer(ply, 'Loading Workshop Bundle silently (no replication): ' .. bundle)
	else
		for bundle in *args
			return VLL2.MessagePlayer(ply, 'Invalid ID provided (' .. bundle .. '). it must be an integer') if not tonumber(bundle)

		local call

		call = ->
			bundle = table.remove(args, 1)
			return if not bundle
			wsid = tostring(math.floor(tonumber(bundle)))\lower()

			if fbundle = VLL2.AbstractBundle\GetLoading(wsid)
				fbundle\AddFinishHook(call)
			else
				fbundle = VLL2.WSBundle(wsid)
				fbundle\AddFinishHook(call)
				fbundle\Load()
				fbundle\DoNotReplicate()

		VLL2.MessagePlayer(ply, 'Loading URL Bundles silently (no replication): ' .. table.concat(args, ', '))
		call()


vll2_workshop_content = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow2(ply)
	bundle = args[1]
	return VLL2.MessagePlayer(ply, 'No workshop ID were specified.') if not bundle
	return VLL2.MessagePlayer(ply, 'Bundle is already loading!') if not VLL2.AbstractBundle\Checkup(bundle\lower())
	return VLL2.MessagePlayer(ply, 'Invalid ID provided. it must be an integer') if not tonumber(bundle)
	fbundle = VLL2.WSBundle(tostring(math.floor(tonumber(bundle)))\lower())
	fbundle\DoNotLoadLua()
	fbundle\Load()
	fbundle\Replicate()
	VLL2.MessagePlayer(ply, 'Loading Workshop Bundle: ' .. bundle .. ' without mounting Lua')

vll2_workshop_content_silent = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow2(ply)
	bundle = args[1]
	return VLL2.MessagePlayer(ply, 'No workshop ID were specified.') if not bundle
	return VLL2.MessagePlayer(ply, 'Bundle is already loading!') if not VLL2.AbstractBundle\Checkup(bundle\lower())
	return VLL2.MessagePlayer(ply, 'Invalid ID provided. it must be an integer') if not tonumber(bundle)
	fbundle = VLL2.WSBundle(tostring(math.floor(tonumber(bundle)))\lower())
	fbundle\DoNotLoadLua()
	fbundle\Load()
	fbundle\DoNotReplicate()
	VLL2.MessagePlayer(ply, 'Loading Workshop Bundle: ' .. bundle .. ' without mounting Lua')

vll2_reload = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow(ply)
	VLL2.MessagePlayer(ply, 'Reloading VLL2, this can take some time...')
	_G.VLL2_GOING_TO_RELOAD = true
	http.Fetch((VLL2.proto .. "://" .. VLL2.host .. "/vll2.lua"), (b) -> _G.RunString(b, "VLL2"))

vll2_reload_full = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow(ply)
	VLL2.MessagePlayer(ply, 'Flly Reloading VLL2, this can take some time...')
	_G.VLL2_GOING_TO_RELOAD = true
	_G.VLL2_FULL_RELOAD = true
	http.Fetch((VLL2.proto .. "://" .. VLL2.host .. "/vll2.lua"), (b) -> _G.RunString(b, "VLL2"))

vll2_clear_lua_cache = (ply, cmd, args) ->
	return VLL2.MessagePlayer(ply, 'Not a super admin!') if disallow(ply)
	sql.Query('DELETE FROM vll2_lua_cache')
	VLL2.MessagePlayer(ply, 'Lua cache has been cleared.')

timer.Simple 0, ->
	if not game.SinglePlayer() or CLIENT
		concommand.Add 'vll2_load', vll2_load, vll2_mkautocomplete('vll2_load')
		concommand.Add 'vll2_github', vll2_github
		concommand.Add 'vll2_gitlab', vll2_gitlab
		concommand.Add 'vll2_workshop', vll2_workshop
		concommand.Add 'vll2_urlcontent', vll2_urlcontent
		concommand.Add 'vll2_urlgma', vll2_urlgma
		concommand.Add 'vll2_wscollection', vll2_wscollection
		concommand.Add 'vll2_wscollection_content', vll2_wscollection_content
		concommand.Add 'vll2_workshop_silent', vll2_workshop_silent
		concommand.Add 'vll2_workshop_content_silent', vll2_workshop_content_silent
		concommand.Add 'vll2_reload', vll2_reload
		concommand.Add 'vll2_reload_full', vll2_reload_full
		concommand.Add 'vll2_clear_lua_cache', vll2_clear_lua_cache

	if SERVER
		net.Receive 'vll2_cmd_load_server', (_, ply) -> vll2_load(ply, nil, net.ReadTable())
		concommand.Add 'vll2_load_server', vll2_load, vll2_mkautocomplete('vll2_load_server')
		concommand.Add 'vll2_github_server', vll2_github
		concommand.Add 'vll2_gitlab_server', vll2_gitlab
		concommand.Add 'vll2_load_silent', vll2_load_silent, vll2_mkautocomplete('vll2_load_silent')
		concommand.Add 'vll2_urlcontent_server', vll2_urlcontent
		concommand.Add 'vll2_urlgma_server', vll2_urlgma
		concommand.Add 'vll2_workshop_server', vll2_workshop
		concommand.Add 'vll2_wscollection_server', vll2_wscollection
		concommand.Add 'vll2_wscollection_content_server', vll2_wscollection_content
		concommand.Add 'vll2_workshop_content_server', vll2_workshop_content
		concommand.Add 'vll2_workshop_silent_server', vll2_workshop_silent
		concommand.Add 'vll2_workshop_content_silent_server', vll2_workshop_content_silent
		concommand.Add 'vll2_reload_server', vll2_reload
		concommand.Add 'vll2_reload_full_server', vll2_reload_full
		concommand.Add 'vll2_clear_lua_cache_server', vll2_clear_lua_cache
	else
		vll2_load_server = (ply, cmd, args) ->
			net.Start('vll2_cmd_load_server')
			net.WriteTable(args)
			net.SendToServer()

		timer.Simple 0, ->
			timer.Simple 0, ->
				if not game.SinglePlayer()
					concommand.Add 'vll2_load_server', vll2_load_server, vll2_mkautocomplete('vll2_load_server')
