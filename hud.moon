
-- Copyright (C) 2018-2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

import VLL2, table, hook, surface, draw, ScrW, ScrH, TEXT_ALIGN_CENTER from _G

bundlelist = {
	VLL2.URLBundle
	VLL2.WSBundle
	VLL2.GMABundle
	VLL2.WSCollection
}

surface.CreateFont('VLL2.Message', {
	font: 'Roboto'
	size: ScreenScale(14)
})

SHADOW = Color(0, 0, 0)
WARN_COLOR = Color(255, 255, 255)
WARN_COLOR2 = Color(80, 80, 80)

hook.Add 'HUDPaint', 'VLL2.HUDMessages', ->
	local messages

	for bundle in *bundlelist
		msgs, progress = bundle\GetMessage()

		if msgs
			messages = messages or {}

			if type(msgs) == 'string'
				table.insert(messages, {msgs, progress}) if msgs ~= ''
			else
				table.insert(messages, {_i, progress}) for _i in *msgs when _i ~= ''

	return if not messages

	x, y = ScrW() / 2, ScrH() * 0.11

	surface.SetFont('VLL2.Message')

	for {message, progress} in *messages
		w, h = surface.GetTextSize(message)
		start = x - w / 2 - 20

		if progress == nil
			draw.DrawText(message, 'VLL2.Message', x + 2, y + 2, SHADOW, TEXT_ALIGN_CENTER)
			draw.DrawText(message, 'VLL2.Message', x, y, WARN_COLOR, TEXT_ALIGN_CENTER)
		elseif progress == 1
			surface.SetDrawColor(71, 215, 37)
			surface.DrawRect(start, y - 5, w + 40, h + 10)
			draw.DrawText(message, 'VLL2.Message', x + 2, y + 2, SHADOW, TEXT_ALIGN_CENTER)
			draw.DrawText(message, 'VLL2.Message', x, y, WARN_COLOR, TEXT_ALIGN_CENTER)
		elseif progress == 0
			surface.SetDrawColor(20, 20, 20)
			surface.DrawRect(start, y - 5, w + 40, h + 10)
			draw.DrawText(message, 'VLL2.Message', x + 2, y + 2, SHADOW, TEXT_ALIGN_CENTER)
			draw.DrawText(message, 'VLL2.Message', x, y, WARN_COLOR2, TEXT_ALIGN_CENTER)
		else
			surface.SetDrawColor(20, 20, 20)
			surface.DrawRect(start, y - 5, w + 40, h + 10)

			surface.SetDrawColor(71, 215, 37)
			surface.DrawRect(start, y - 5, (w + 40) * progress, h + 10)

			draw.DrawText(message, 'VLL2.Message', x + 2, y + 2, SHADOW, TEXT_ALIGN_CENTER)

			render.SetScissorRect(0, 0, start + (w + 40) * progress, ScrH(), true)
			draw.DrawText(message, 'VLL2.Message', x, y, WARN_COLOR, TEXT_ALIGN_CENTER)

			render.SetScissorRect(start + (w + 40) * progress, 0, ScrW(), ScrH(), true)
			draw.DrawText(message, 'VLL2.Message', x, y, WARN_COLOR2, TEXT_ALIGN_CENTER)
			render.SetScissorRect(0, 0, 0, 0, false)

		y += h * 1.1
